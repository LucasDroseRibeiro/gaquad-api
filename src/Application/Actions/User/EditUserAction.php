<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;

class EditUserAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $userId = (int) $this->resolveArg('id');
        $user = $this->userRepository->findUserOfId($userId);

        $parsedBody = $this->request->getParsedBody();

        $admin = isset($parsedBody['admin']) ? intval($parsedBody['admin']) : 0;

        if (empty($parsedBody['email'])){
            throw new HttpNotFoundException($this->request, "E-mail é obrigatorio");
        }
        if (empty($parsedBody['nome'])){
            throw new HttpNotFoundException($this->request, "Nome é obrigatorio");
        }
        if (empty($parsedBody['cpf'])){
            throw new HttpNotFoundException($this->request, "CPF é obrigatorio");
        }
        
        if (!isset($parsedBody['senha']))
            $parsedBody['senha'] = "";

        $user = $this->userRepository->update(
            $userId,
            strval($parsedBody['email']),
            strval($parsedBody['nome']),
            strval($parsedBody['cpf']),
            strval($parsedBody['senha']),
            intval($admin)
        );

        $this->logger->info("Usuário foi atualizado.");

        return $this->respondWithData($user);
    }
}
