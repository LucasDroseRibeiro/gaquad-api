<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpUnauthorizedException;
use Slim\Exception\HttpBadRequestException;
use App\Application\Actions\User;

class CreateUserAgendaAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $user_id = (int) $this->resolveArg('id');
        $user = $this->userRepository->findUserOfId($user_id);
        
        $userTokenId = $this->request->getAttribute('user_id');
        $userToken = $this->userRepository->findUserOfId($userTokenId);
        
        if ($user_id != $userTokenId)
            if (!$userToken->getAdmin())
                throw new HttpUnauthorizedException($this->request);

        $parsedBody = $this->request->getParsedBody();

        $agendas = $this->agendaRepository->findAll();
        foreach ($agendas as $agenda){

            $dataPtBr = new \DateTime(strval($parsedBody['data']));
            $dataPtBr->format("d/m/Y");

            if ($agenda->getData() == $dataPtBr->format("d/m/Y") ){
                $horas = json_decode(strval($parsedBody['horas']));
                foreach ( $horas as $hora){
                    if ( in_array($hora, $agenda->getHoras()) )
                        throw new HttpBadRequestException($this->request);
                }
            }
        }
        
        $agenda = $this->agendaRepository->create(
            $user->getId(),
            intval($parsedBody['quadra_id']),
            strval($parsedBody['data']),
            strval($parsedBody['horas']));

            $this->logger->info("Agendamento de Usuário foi criado.");

        return $this->respondWithData($agenda);
    }
}
