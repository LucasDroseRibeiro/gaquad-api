<?php
declare(strict_types=1);

namespace App\Application\Actions\User;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;

class CreateUserAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $parsedBody = $this->request->getParsedBody();

        $admin = isset($parsedBody['admin']) ? intval($parsedBody['admin']) : 0;

        if (empty(strval($parsedBody['email']))){
            throw new HttpNotFoundException($this->request, "E-mail é obrigatorio");
        }
        if (empty(strval($parsedBody['nome']))){
            throw new HttpNotFoundException($this->request, "Nome é obrigatorio");
        }
        if (empty(strval($parsedBody['cpf']))){
            throw new HttpNotFoundException($this->request, "CPF é obrigatorio");
        }
        if (empty(strval($parsedBody['senha']))){
            throw new HttpNotFoundException($this->request, "Senha é obrigatorio");
        }

        $user = $this->userRepository->create(
            strval($parsedBody['email']),
            strval($parsedBody['nome']),
            strval($parsedBody['cpf']),
            strval($parsedBody['senha']),
            $admin
        );

        $this->logger->info("Usuário foi criado.");

        return $this->respondWithData($user);
    }
}
