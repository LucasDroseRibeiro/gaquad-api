<?php
declare(strict_types=1);

namespace App\Application\Actions\Auth;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;

class CreateAuthAction extends AuthAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $parsedBody = $this->request->getParsedBody();

        if (empty($parsedBody['email'])){
            throw new HttpNotFoundException($this->request, "E-mail é obrigatorio");
        }
        if (empty($parsedBody['senha'])){
            throw new HttpNotFoundException($this->request, "Senha é obrigatorio");
        }

        $user = $this->userRepository->findUserByEmailAndSenha(
            strval($parsedBody['email']),
            strval($parsedBody['senha']));

        $auth = $this->authRepository->create($user->getId());

        $this->logger->info("Autenticação foi criado.");

        return $this->respondWithData([
            'auth'=> $auth,
            'user'=> $user
        ]);
    }
}
