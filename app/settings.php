<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {
    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'displayErrorDetails' => true, // Should be set to false in production
            'logger' => [
                'name' => 'slim-app',
                'path' => 'php://stdout',
                'level' => Logger::DEBUG,
            ],
            /*'pdo' => [
                'user' => '',
                'password' => '',
                'database' => 'sqlite:../database.sqlite3'
            ],*/
            'pdo' => [
                'user' => 'api',
                'password' => '@pi#2020',
                'database' => 'mysql:host=gaquad.servehttp.com;dbname=gaquad'
            ],
        ],
    ]);
};
