/*
MYSQL
CREATE USER 'api'@'%' IDENTIFIED BY '@pi#2020';
GRANT ALL PRIVILEGES ON gaquad.* TO 'api'@'%';
FLUSH PRIVILEGES;

use gaquad;

CREATE TABLE auth (id int primary key auto_increment, user_id int, token text, validade int);
CREATE TABLE quadras (id int primary key auto_increment, nome text, preco int);
CREATE TABLE agendas (id int primary key auto_increment, user_id int, quadra_id int, data text, horas text);
CREATE TABLE users (id int primary key auto_increment, email text, nome text, cpf text, senha text, admin int);
*/

/*
SQLITE
CREATE TABLE auth (id integer primary key autoincrement, user_id integer, token text, validade integer);
CREATE TABLE quadras (id integer primary key autoincrement, nome text, preco integer);
CREATE TABLE agendas (id integer primary key autoincrement, user_id integer, quadra_id integer, data text, horas text);
CREATE TABLE users (id integer primary key autoincrement, email text, nome text, cpf text, senha text, admin integer);
DELETE FROM sqlite_sequence;
*/